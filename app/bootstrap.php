<?php

$loader = require_once __DIR__.'/../vendor/autoload.php';

$loader->add('TheIconic', __DIR__.'/../src');

return $loader;
