<?php

ini_set('display_errors', 1);

$loader = require_once __DIR__.'/../app/bootstrap.php';

$loader->add('TheIconic\\Tests', __DIR__);

$loader->register();
