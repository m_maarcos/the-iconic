<?php

namespace TheIconic\Tests\Services;

use TheIconic\Exception\APIException;
use TheIconic\Entities\Product;
use TheIconic\Services\ProductsService;

class ProductServiceTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException TheIconic\Exception\APIException
     * @expectedExceptionCode 404
     */
    function testFailingFetchProduct() {
        ProductsService::fetchProduct(uniqid());
    }

    function testFetchAllProducts($q = null, $page = 1) {
        $result = ProductsService::fetchProducts($q, $page);
        $products = $result->getProducts();

        $this->assertTrue($products > 0);

        return $products;
    }

    /**
     * @depends testFetchAllProducts
     */
    function testFetchProduct(array $products) {
        $product = ProductsService::fetchProduct($products[0]->sku);

        $this->assertInstanceOf(Product::class, $product);
    }

    function testFetchProductWithSearch() {
        $this->testFetchAllProducts('shirt');
    }

    function testFetchProductWithPagination() {
        $this->testFetchAllProducts(rand());
    }
}
