$.fn.extend({
    photoViewer : function(images) {
        var element = $(this).first();
        var imgElement = $('img', element).first();
        var imagesJSON = JSON.parse(images);
        var currentIndex = 0;

        function setImageWithIndexOffset(offset) {
            let count = imagesJSON.length;
            var idx = currentIndex + offset;

            if (idx < 0) {
                idx = count + idx;
            }
            else if (idx >= count) {
                idx = idx - count;
            }

            currentIndex = idx;

            $(imgElement).attr('src', imagesJSON[currentIndex].thumbnail);
        }

        $('a.previous', this).click(function() {
            setImageWithIndexOffset(-1);
        });

        $('a.next', this).click(function() {
            setImageWithIndexOffset(1);
        });
    }
});