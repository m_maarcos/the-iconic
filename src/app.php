<?php

use Silex\Application;
use Silex\Provider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;

define('__DEBUG__', false);

$app = new Application();

$app['debug'] = __DEBUG__;

$app->register(new UrlGeneratorServiceProvider());
$app->register(new ValidatorServiceProvider());
$app->register(new ServiceControllerServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());

$app->register(new Provider\WebProfilerServiceProvider(), array(
    'profiler.cache_dir' => __DIR__ . '/../cache/profiler',
    'profiler.mount_prefix' => '/_profiler',
));

$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {

    $twig->addFunction(new Twig_SimpleFunction('get', function ($object, $property) {
        return $object->$property;
    }));

    return $twig;
}));

$app['twig.path'] = array(__DIR__.'/../templates');
$app['twig.options'] = array('cache' => __DEBUG__ ? false : __DIR__.'/../var/cache/twig');

return $app;
