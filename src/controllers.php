<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use TheIconic\Services\ProductsService;

$app->get('/', function () use ($app) {

    $productResponse = ProductsService::fetchProducts($app['request']->get('q'));

    return $app['twig']
        ->resolveTemplate('index.html')
        ->render(array(
            'links' => $productResponse->getLinks(),
            'page_size' => $productResponse->page_size,
            'total_items' => (int)$productResponse->total_items,
            'products' => $productResponse->getProducts(),
        ));
});

$app->get('/product/{sku}', function ($sku) use ($app) {

    $product = ProductsService::fetchProduct($sku);

    $jsonImages = json_encode(array_map(function($obj) {
        return $obj->getRawBody();
    }, $product->getImages()));

    return $app['twig']
        ->resolveTemplate('product.html')
        ->render(array(
            'product' => $product,
            'jsonImages' => $jsonImages,
        ));
});


// ERROR HANDLER
$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    $code = $e->getCode() ?: $code;

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html',
        'errors/'.substr($code, 0, 2).'x.html',
        'errors/'.substr($code, 0, 1).'xx.html',
        'errors/default.html',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});
