<?php

namespace TheIconic\Services;

use TheIconic\Exception\APIException;
use TheIconic\Entities\Product;
use TheIconic\ProductsAPIResponse;

abstract class ProductsService extends BaseService {

    /**
     * Fetch all products from API
     * @param string|null $query A text simple search
     * @param int $page The page number to return (defaults to 1)
     * @return An instance of TheIconic\ProductsAPIResponse
     */
    public static function fetchProducts($query = null, $page = 1) {
        $result = self::sendRequest('catalog/products', array(
            'q' => $query,
            'page' => $page,
        ));

        return new ProductsAPIResponse($result);
    }

    /**
     * Fetch a single product from API by the SKU
     * @param string $sku The product SKU
     * @return An instance of TheIconic\Entities\Product
     */
    public static function fetchProduct($sku) {
        $result = self::sendRequest('catalog/products/' . $sku);

        if ($json = json_decode($result)) {
            if (isset($json->sku)) {
                return new Product($json);
            }

            throw new APIException('Item not found', 404);
        }

        throw new APIException('Malformed API response', 500);
    }

}