<?php

namespace TheIconic\Services;

use TheIconic\APIResponse;
use TheIconic\Exception\APIException;

abstract class BaseService {

    private static $host = 'https://eve.theiconic.com.au/v1/';

    /**
     * Sends cURL request to THE ICONIC API
     * @param string $endpoint The endpoint to be called
     * @param array $params An array of query parameters
     * @return string
     */
    final static protected function sendRequest($endpoint, array $params = array()) {
        $curl = curl_init();

        $urlQueryString = http_build_query($params);
        $url = self::$host . $endpoint . '?' . $urlQueryString;

        curl_setopt_array($curl, array(
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url
        ));

        if ($response = curl_exec($curl)) {
            return $response;
        }

        throw new APIException('Failed to fetch data');
    }

}