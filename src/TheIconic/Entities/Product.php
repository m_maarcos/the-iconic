<?php

namespace TheIconic\Entities;

use TheIconic\APIObject;
use TheIconic\ProductsAPIResponse;

class Product extends APIObject {

    private $gender;
    private $brand;
    private $images;
    private $related_products = array();

    /**
     * Creates a product from the JSON returned by the API
     * @param stdClass $jsonProduct The parsed JSON
     * @return TheIconic\Product
     */
    public function __construct(\stdClass $jsonProduct) {
        parent::__construct($jsonProduct);

        if ($embedded = $this->getEmbedded()) {
            $this->gender = new APIObject($embedded->gender);
            $this->brand = new APIObject($embedded->brand);
            $this->images = APIObject::parseAPIObjects($embedded->images);

            if (isset($embedded->related_products)) {
                $this->related_products = ProductsAPIResponse::parseProducts($embedded->related_products);
            }
        }
    }

    public function getGender() {
        return $this->gender;
    }

    public function getBrand() {
        return $this->brand;
    }

    public function getImages() {
        return $this->images;
    }

    public function getRelatedProducts() {
        return $this->related_products;
    }

}
