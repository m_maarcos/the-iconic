<?php

namespace TheIconic;

use TheIconic\Entities\Product;
use TheIconic\APIObject;

class ProductsAPIResponse extends APIResponse {

    private $products;
    private $facets;

    public function __construct($rawResponse) {
        parent::__construct($rawResponse);
    }

    public function getProducts() {
        if (!$this->products) {
            // Only parsing products on get in order to avoid unnecessary processing
            $products = isset($this->getEmbedded()->product) ? $this->getEmbedded()->product : array();
            $this->products = self::parseProducts($products);
        }

        return $this->products;
    }

    public function getFacets() {
        if (!$this->facets) {
            // Only parsing facets on get in order to avoid unnecessary processing
            $this->facets = APIObject::parseAPIObjects($this->getEmbedded()->facets);
        }

        return $this->facets;
    }

    /**
     * Parse an array of stdClass objects into an array of TheIconic\Entities\Product objects
     * @param array $products The array to be parsed
     * @return array
     */
    public static function parseProducts(array $products) {

        if (($count = count($products)) > 0) {
            for ($i=0; $i<$count; $i++) {
                $products[$i] = new Product($products[$i]);
            }
        }

        return $products;
    }

}