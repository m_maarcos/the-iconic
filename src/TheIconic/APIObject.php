<?php

namespace TheIconic;

class APIObject {

    protected $rawBody;

    public function __construct(\stdClass $rawBody) {
        $this->rawBody = $rawBody;
    }

    public function __get($key) {
        return $this->rawBody->$key ?: null;
    }

    public function getEmbedded() {
        return isset($this->rawBody->_embedded) ? $this->rawBody->_embedded : null;
    }

    public function getLinks() {
        return $this->rawBody->_links;
    }

    public function getRawBody() {
        return $this->rawBody;
    }

    /**
     * Parse an array of stdClass objects into an array of TheIconic\APIObject objects
     * @param array $objects The array to be parsed
     * @return array
     */
    public static function parseAPIObjects(array $objects) {
        $parsed = array();

        for ($i=0; $i<count($objects); $i++) {
            $obj = $objects[$i];

            if ($obj instanceof \stdClass) {
                $parsed[] = new APIObject($obj);
            }
        }

        return $parsed;
    }

}