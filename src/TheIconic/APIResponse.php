<?php

namespace TheIconic;

class APIResponse extends APIObject {

    private $page_count;
    private $page_size;
    private $total_items;

    public function __construct($rawResponse) {
        $parsedResponse = is_string($rawResponse) ? json_decode($rawResponse) : $rawResponse;

        parent::__construct($parsedResponse);

        if ($parsedResponse) {
            $this->page_count = $parsedResponse->page_count;
            $this->page_size = $parsedResponse->page_size;
            $this->total_items = $parsedResponse->total_items;
        }
        else {
            throw new TheIconic\Exception\APIException('Malformed response body', 500);
        }
    }

    public function getPageCount() {
        return $this->page_count;
    }

    public function getPageSize() {
        return $this->page_size;
    }

    public function getTotalItems() {
        return $this->total_items;
    }

}